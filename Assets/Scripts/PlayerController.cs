using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour {

    [SerializeField] PlayerInputsReceiver _input;
    [SerializeField] PlayerInput pInput;
    public void SwithActionMap(string targetMap = "") { pInput.SwitchCurrentActionMap(targetMap); }
    public PlayerInputsReceiver input => _input;

    Vector2 moveInput;

    [SerializeField] Animator _anim;
    [SerializeField] Rigidbody _rb;

    [SerializeField] float moveSpeed = 10f;

    [Header("�v���C���[�������Ă��邩")]
    public bool isVisible = false;
    RaycastHit lastVisibleStateRayHit;
    RaycastHit visibleStateRayHit;
    [SerializeField]LayerMask visibleStateLayerMask;
    [SerializeField] Transform visibleCenterTrns;

    public PlayerFootstepsSound footstepsSound;

    // Start is called before the first frame update
    void Start() {
        if (!_input) TryGetComponent(out _input);
        if (!_rb) TryGetComponent(out _rb);
    }

    // Update is called once per frame
    void Update() {
        if (_input) {
            moveInput = _input.rawMove;

            if (input.rawMove.magnitude == 0) footstepsSound.Stop();
        }

        isVisible = GetVisibleState();
        if (lastVisibleStateRayHit.collider != visibleStateRayHit.collider) {
            if (!isVisible) {
                if (visibleStateRayHit.collider != null) {
                    var _c = visibleStateRayHit.collider.GetComponent<TestDisableSpriteWhilePlayerHide>();
                    if (_c) {
                        _c.Deactivate();
                    }
                }
            } else {
                if (lastVisibleStateRayHit.collider != null) {
                    var _c = lastVisibleStateRayHit.collider.GetComponent<TestDisableSpriteWhilePlayerHide>();
                    if (_c) {
                        _c.Activate();
                    }
                }
            }
            lastVisibleStateRayHit = visibleStateRayHit;
        }

        ApplyAnimationParams();
    }

    private void FixedUpdate() {
        if (_rb) {
            _rb.velocity = new Vector3(moveInput.x, 0, moveInput.y) * moveSpeed * 100f * Time.deltaTime;
        }
    }

    private void ApplyAnimationParams() {
        if (!_anim) return;

        _anim.SetFloat("Speed", moveInput.magnitude);
        _anim.SetFloat("InputX", moveInput.x);
        _anim.SetFloat("InputY", moveInput.y);
        _anim.SetFloat("InputRadians", Mathf.Atan2(moveInput.y, moveInput.x));
    }

    private bool GetVisibleState() {
        if (visibleCenterTrns == null) {
            return false;
        }

        return !Physics.Linecast(visibleCenterTrns.position, Camera.main.transform.position, out visibleStateRayHit, visibleStateLayerMask);
    }
}