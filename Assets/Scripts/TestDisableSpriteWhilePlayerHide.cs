using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDisableSpriteWhilePlayerHide : MonoBehaviour {

    [SerializeField] bool _enabled = true;

    [SerializeField]SpriteRenderer _renderer;

    float m_alpha = 0;
    float alpha {
        get {
            return Mathf.Clamp01(m_alpha);
        }
        set {
            m_alpha = Mathf.Clamp01(value);
        }
    }

    int targetAlpha = 0;

    public void Activate() {
        if (!_enabled) return;

        StopCoroutine("Fade");
        targetAlpha = 1;
        StartCoroutine("Fade");
    }

    public void Deactivate() {
        if (!_enabled) return;

        StopCoroutine("Fade");
        targetAlpha = 0;
        StartCoroutine("Fade");
    }

    IEnumerator Fade() {
        if (!_renderer) yield break;

        var tmpCol = _renderer.color;

        alpha = tmpCol.a;

        while (true) {
            if (alpha != targetAlpha) {
                if (alpha < targetAlpha) {
                    alpha += Time.deltaTime * 5;
                } else if (alpha > targetAlpha) {
                    alpha -= Time.deltaTime * 5;
                }

                tmpCol.a = alpha;
                _renderer.color = tmpCol;
            } else {
                break;
            }

            yield return null;
        }

        tmpCol.a = targetAlpha;
        _renderer.color = tmpCol;

        yield break;
    }
}