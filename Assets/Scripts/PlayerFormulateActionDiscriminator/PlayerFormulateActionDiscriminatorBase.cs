using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerFormulateActionDiscriminator {
    public abstract class PlayerFormulateActionDiscriminatorBase {
        protected PlayerInputsReceiver input => FormulateExeManager.instance.input;

        public abstract bool UpdateState();
    }
}