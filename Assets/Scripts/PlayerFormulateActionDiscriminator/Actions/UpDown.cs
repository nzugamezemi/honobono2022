using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerFormulateActionDiscriminator {
    public class UpDown : PlayerFormulateActionDiscriminatorBase {

        float x, y;

        float m_yAcceptThreshold = 0.8f;
        float m_xOutThreshold = 0.4f;

        bool m_upState = false;
        bool m_downState = false;

        int m_upCount = 0;
        int m_downCount = 0;

        // 往復回数
        int m_roundTripCount = 0;
        // 往復管理用
        bool startRoundTripWithUp = false;
        int tmp_up_count = 0;
        int tmp_down_count = 0;
        void ResetRoundTripTempParam() { tmp_up_count = tmp_down_count = 0; }

        public float yAcceptThreshold => m_yAcceptThreshold;
        public float xOutThreshold => m_xOutThreshold;

        public bool upState => m_upState;
        public bool downState => m_downState;
        public int upCount => m_upCount;
        public int downCount => m_downCount;

        public int roundTripCount => m_roundTripCount;

        // 切り替わり時のコールバック予定
        // UnityEngine.Events.UnityAction on;

        public UpDown(float yAcceptThreshold = 0.8f, float xOutThreshold = 0.4f) {
            m_yAcceptThreshold = yAcceptThreshold;
            m_xOutThreshold = xOutThreshold;

            m_upState = false;
            m_downState = false;
        }

        public override bool UpdateState() {
            x = input.rawMove.x;
            y = input.rawMove.y;

            bool _result = false;

            if (!IsOutOfRange()) {   // 横が外れていない
                if (Mathf.Abs(y) >= m_yAcceptThreshold) {
                    if (!m_upState && !m_downState) {   // 上下入力がまだない場合はどっちでも受け付け
                        // y入力が上方向か
                        m_upState = IsUp(y);
                        // downはupの逆になるはず
                        m_downState = !m_upState;

                        if (m_upState) {
                            startRoundTripWithUp = true;
                            m_upCount++;
                        } else if(m_downState) {
                            m_downCount++;
                        }

                    } else {   // すでに上か下の入力がある場合
                        // 入力切替の前に、反対側の入力があったかを同時に判定
                        if (IsUp(y)) {
                            InputUpSide();
                        } else if (m_upState) {
                            InputDownSide();
                        }
                    }

                    // 往復チェック
                    _result = CheckRoundTrip();
                }

            } else {
                OnOutOfRange();
                _result = false;
            }

            return _result;
        }

        bool IsUp(float y) {
            // y入力が0以上なら上方向
            return y > 0;
        }

        void InputUpSide() {
            if (!m_upState && m_downState) {
                m_upState = true;
                m_downState = false;
                m_upCount++;
            }
        }

        void InputDownSide() {
            if (!m_downState && m_upState) {
                m_downState = true;
                m_upState = false;
                m_downCount++;
            }
        }

        public bool IsOutOfRange() {
            return Mathf.Abs(x) > m_xOutThreshold;
        }

        void OnOutOfRange() {
            m_downState = false;
            m_upState = false;

            m_upCount = 0;
            m_downCount = 0;

            startRoundTripWithUp = false;

            m_roundTripCount = 0;

            ResetRoundTripTempParam();
        }

        /// <summary>
        /// スティックの往復検知
        /// </summary>
        /// <returns></returns>
        bool CheckRoundTrip() {
            int _count1 = startRoundTripWithUp ? m_upCount : m_downCount;
            if (_count1 > 1) {
                int _count2 = startRoundTripWithUp ? tmp_up_count : tmp_down_count;

                if (_count1 - _count2 > 0) {
                    tmp_up_count = m_upCount;
                    tmp_down_count = m_downCount;

                    m_roundTripCount++;

                    return true;
                    // Debug.Log("往復");
                }
            }

            return false;
        }
    }
}