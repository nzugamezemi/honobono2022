using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerFormulateActionDiscriminator {
    public class Rotate : PlayerFormulateActionDiscriminatorBase {

        bool beginInput = false;

        public int continuousRotNum = 0;
        public int totalRotNum = 0;

        public enum RotateDirection {
            N, L, R
        }
        public RotateDirection rotateDirection;
        RotateDirection lastRotateDirection;

        public int sumDeg = 0;

        Vector2 startInputVec = Vector2.zero;
        Vector2 inputVec = Vector2.zero;
        Vector2 lastInputVec = Vector2.zero;

        public string GetRotateDirectionStr() {
            string _str = "Null";

            switch (rotateDirection) {
                case RotateDirection.N:
                    _str = "ニュートラル";
                    break;
                case RotateDirection.R:
                    _str = "右回転";
                    break;
                case RotateDirection.L:
                    _str = "左回転";
                    break;
            }

            return _str;
        }

        public override bool UpdateState() {
            inputVec = input.rawMove;

            // 入力中
            if (inputVec.normalized != Vector2.zero && inputVec.magnitude > 0.98f) {
                // 入力開始
                if (!beginInput) {
                    lastInputVec = inputVec;
                    startInputVec = inputVec;

                    lastRotateDirection = RotateDirection.N;
                    rotateDirection = RotateDirection.N;
                    sumDeg = 0;

                    beginInput = true;
                }

                float delta = Vector2.SignedAngle(lastInputVec, inputVec) / 180;

                if (Mathf.Abs(delta) > 0.05f) {
                    if (delta > 0) {
                        rotateDirection = RotateDirection.L;
                    } else if (delta < 0) {
                        rotateDirection = RotateDirection.R;
                    }

                    if (lastRotateDirection != rotateDirection) {
                        sumDeg = 0;
                        continuousRotNum = 0;
                        lastRotateDirection = rotateDirection;
                    } else {
                        sumDeg += (Mathf.Abs(delta) * 180).RoundOff();

                        if (sumDeg >= 360) {
                            sumDeg -= 360;
                            continuousRotNum++;
                            totalRotNum++;
                        }
                    }

                    // Debug.Log(sumDeg + ", " + continuousRotNum + ", " + totalRotNum +", " + rotateDirection);

                    lastInputVec = inputVec;
                }
            } else {
                // 入力されていない
                ResetRotateState();
            }

            return false;
        }

        void ResetRotateState() {
            rotateDirection = RotateDirection.N;

            sumDeg = 0;
            continuousRotNum = 0;

            beginInput = false;
        }

        int CalcDegree(float x, float y, int offset = 0) {
            int _deg = (Mathf.Atan2(y, x) * Mathf.Rad2Deg).RoundOff();
            if (_deg < 0) {
                _deg += 360;
            }
            return _deg - offset;
        }
    }
}