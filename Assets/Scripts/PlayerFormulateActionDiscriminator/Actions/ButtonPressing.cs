using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerFormulateActionDiscriminator {
    public class ButtonPressing : PlayerFormulateActionDiscriminatorBase {

        bool m_btnState = false;

        int m_repeatedCount = 0;   // �A�ŉ�

        public bool btnState => m_btnState;
        public int repeatedCount => m_repeatedCount;

        public override bool UpdateState() {
            if (m_btnState != input.fire0) {
                m_btnState = input.fire0;
                if (m_btnState) {
                    m_repeatedCount++;
                    return true;
                }
            }

            // throw new System.NotImplementedException();

            return false;
        }
    }
}