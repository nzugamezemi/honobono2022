using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerFormulateActionDiscriminator {
    public class DPadPressing : PlayerFormulateActionDiscriminatorBase {

        bool m_btnState = false;

        int m_repeatedCount = 0;   // 連打回数

        public bool btnState => m_btnState;
        public int repeatedCount => m_repeatedCount;

        enum Direction {
            Neutral=-1,
            Up,
            Down,
            Right,
            Left
        }

        Direction currentInputDirection = Direction.Neutral;
        Direction lastInputDirection = Direction.Neutral;

        public override bool UpdateState() {
            Vector2Int snappedInput = SnapInput(input.rawMove.normalized);
            if (snappedInput.magnitude > 0.001f) {
                currentInputDirection = GetDirectionFromVector(snappedInput);
            } else {
                currentInputDirection = Direction.Neutral;
                lastInputDirection = Direction.Neutral;
            }

            if (lastInputDirection != currentInputDirection) {
                if (currentInputDirection == Direction.Neutral && lastInputDirection != Direction.Neutral) {
                    return false;
                }
                //Debug.Log($"D-Pad 入力 : Current[{currentInputDirection}], Last[{lastInputDirection}]");
                lastInputDirection = currentInputDirection;
                return true;
            }

            return false;
        }

        private Vector2Int SnapInput (Vector2 rawInput) {
            int x = 0;
            int y = 0;

            if (rawInput.x >= 0.5f) {
                x = 1;
            } else if (rawInput.x <= 0.5f && rawInput.x < 0) {
                x = -1;
            }

            if (rawInput.y >= 0.5f) {
                y = 1;
            } else if (rawInput.y <= 0.5f && rawInput.y < 0) {
                y = -1;
            }

            return new Vector2Int(x, y);
        }

        private Direction GetDirectionFromVector(Vector2 vec) {
            Direction _result;
            switch (vec) {
                case Vector2 v when v.Equals(Vector2.up):
                    _result = Direction.Up;
                    break;
                case Vector2 v when v.Equals(Vector2.down):
                    _result = Direction.Down;
                    break;
                case Vector2 v when v.Equals(Vector2.right):
                    _result = Direction.Right;
                    break;
                case Vector2 v when v.Equals(Vector2.left):
                    _result = Direction.Left;
                    break;
                default:
                    _result = Direction.Neutral;
                    break;
            }

            return _result;
        }
    }
}