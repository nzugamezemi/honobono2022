using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FloatExtensions {

    /// <summary>
    /// 四捨五入します。
    /// Round off.
    /// </summary>
    public static int RoundOff(this float self) {
        float dec = self - Mathf.FloorToInt(self);   // 整数部分を削除
        int intege = Mathf.FloorToInt(dec * 10);   // 小数第一位部分

        // 小数第一位部分が5以上なら切り上げ、以下なら切り捨て
        if (intege >= 5)
            return Mathf.CeilToInt(self);
        else
            return Mathf.FloorToInt(self);
    }
}
