using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GrassPlacer : MonoBehaviour {

    bool running = false;

    [SerializeField] GameObject grassPrefab;
    [SerializeField] float range = 500;
    [SerializeField] int number = 5000;
    [SerializeField] float sizeRange = 0.1f;

    List<GameObject> instances = new List<GameObject>();

    public void PlaceGrass() {
        if (!grassPrefab || running) return;

        running = true;

        RemoveAllGrass();

        instances = new List<GameObject>();

        for (int i = 0; i < number; i++) {
            Vector3 pos = new Vector3(Random.Range(-range, range), 0, Random.Range(-range, range));
            Quaternion rot = Quaternion.Euler(45, 0, 0);

#if UNITY_EDITOR
            var _go = PrefabUtility.InstantiatePrefab(grassPrefab, transform) as GameObject;
            _go.transform.position = pos;
            _go.transform.rotation = rot;
            Vector3 scale = _go.transform.localScale * (1 + Random.Range(-sizeRange, sizeRange));
            _go.transform.localScale = scale;
            instances.Add(_go);

            float p = Random.Range(0f, 1f);
            if (p > 0.5f) {
                _go.GetComponent<SpriteRenderer>().flipX = true;
            }

            _go.hideFlags = HideFlags.HideInHierarchy;
#endif
        }

        running = false;
    }

    void RemoveAllGrass() {
        foreach (GameObject child in instances) {
            DestroyImmediate(child.gameObject);
        }
        instances.Clear();
    }

    public void DestroyChildren(Transform parent) {
        for (var i = parent.childCount - 1; i >= 0; i--) {
            Object.DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }
}