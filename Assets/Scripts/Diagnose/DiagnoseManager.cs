using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;

namespace Diagnose {
    public class DiagnoseManager : SingletonMonoBehaviour<DiagnoseManager> {

        [SerializeField] DataAccessUnit DAU;

        [SerializeField]
        ItemRecipeDatabase itemRecipeDatabase;

        [SerializeField] DiagnoseDatabase database;

        [SerializeField]
        Patient m_patient;
        public Patient patient => m_patient;
        public void SetPatient(Patient newPatient) => m_patient = newPatient;

        // 患者
        [SerializeField] GameObject patientObj;
        [SerializeField] SpriteRenderer patientRenderer;

        // 患者待ち中
        bool isWaitingForPatient = false;
        [SerializeField] Button waitForPatientBtn;

        [SerializeField] GameObject loadingPanel;

        // 質問中
        bool isAskingQuestions = false;
        // 質問したかどうかを保持
        List<bool> answeredQuestions = new List<bool>();
        // 最後に質問したボタンのインデックスを保持
        int lastQuestionBtnIndex = -1;

        // 質問用ボタン
        [SerializeField] GameObject questionBtnsParent;
        [SerializeField] List<Button> questionBtns = new List<Button>();

        // 調合シーンへの移動ボタン
        [SerializeField] Button goNextSceneBtn;

        // 会話ログ用
        [Header("会話ログ用設定")]
        [SerializeField] ConversationManager conversationManager;

        void Start() {
            // initializing
            Initialize();

            EnterDiagnose();
        }

        void Initialize() {
            patientObj.SetActive(false);

            SetupPatientData();

            isWaitingForPatient = false;
            waitForPatientBtn.onClick.RemoveAllListeners();
            waitForPatientBtn.onClick.AddListener(StartWaitForPatient);

            isAskingQuestions = false;

            DeactivateQuestionButtonGroup();
            answeredQuestions = new List<bool>();
            answeredQuestions.Clear();
            for (int i = 0; i < questionBtns.Count; i++) answeredQuestions.Add(false);
            answeredQuestions.ForEach(i => i = false);

            goNextSceneBtn.onClick.AddListener(() => {
                UnityEngine.SceneManagement.SceneManager.LoadScene("IngredientsSelectionScene");
            });
            goNextSceneBtn.gameObject.SetActive(false);
        }

        void SetupPatientData() {
            if (!database) return;

            // 患者ランダム化
            var patientsList = database.AllPatients;
            SetPatient(patientsList[Random.Range(0, patientsList.Count)]);

            // データ格納
            if (patient) {
                patientRenderer.sprite = patient._diseaseSprite;

                DAU.data.patient = patient;
                DAU.data.targetRecipe = itemRecipeDatabase.GetRecipeDataFromMedicineData(patient.medicine);
            }
        }

        public void EnterDiagnose() {
            // Debug.Log("診断処理開始");

            StartCoroutine("StartDiagnose");
        }

        IEnumerator StartDiagnose() {

            // ボタン押下待ち
            waitForPatientBtn.Select();

            yield return new WaitUntil(() => isWaitingForPatient == true);

            // Debug.Log("Loading");
            loadingPanel.SetActive(true);

            yield return new WaitForSeconds(2f);

            EnterPatient();

            // 患者表示
            patientObj.SetActive(true);

            // ロード画面非表示
            loadingPanel.SetActive(false);

            yield return new WaitUntil(() => isWaitingForPatient == false);

            yield return new WaitForSeconds(.5f);

            // 挨拶
            // シナリオセット
            conversationManager.scenarioLines = patient.enterGreetingScenarios;
            yield return StartCoroutine(conversationManager.ConversationIE());

            // 選択肢ボタンのセットアップ
            string[] choises = patient.questions;
            for (int i = 0; i < questionBtns.Count; i++) {
                int _index = i;
                Button _btn = questionBtns[_index];
                _btn.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = choises[_index];
                _btn.onClick.RemoveAllListeners();
                _btn.onClick.AddListener(() => DisplayAnswer(_index));
            }

            yield return new WaitForSeconds(.5f);

            ActivateQuestionButtonGroup();
            questionBtns[0].Select();

            goNextSceneBtn.gameObject.SetActive(true);

            // 質問フェーズ開始
            while (true) {
                yield return new WaitUntil(() => isAskingQuestions);   // 会話モードに入るまで待機
                yield return StartCoroutine(conversationManager.ConversationIE());   // 会話終了待機

                // 選択肢ボタン復活
                if (questionBtns.Any(i => i.interactable == false)) {
                    ActivateQuestionButtonGroup();
                    questionBtns[lastQuestionBtnIndex].Select();

                    isAskingQuestions = false;
                }

                // 全ての選択肢を質問したかチェック
                if (answeredQuestions.All(i => i == true)) {
                    // 抜ける
                    // break;

                    // 診察終わったフラグ
                    DAU.data.ignoreDiagnose = true;

                    // 次に進めるように
                    goNextSceneBtn.interactable = true;
                }

                yield return null;
            }
        }

        public void StartWaitForPatient() {
            // Debug.Log("患者を待ちます");
            isWaitingForPatient = true;
            waitForPatientBtn.gameObject.SetActive(false);
        }

        public void EnterPatient() {
            // Debug.Log("患者が来ました");
            isWaitingForPatient = false;
        }

        void DisplayAnswer(int index) {
            // 質問回答中はボタンをオフに
            DeactivateQuestionButtonGroup();

            answeredQuestions[index] = true;

            lastQuestionBtnIndex = index;

            // シナリオセット
            conversationManager.scenarioLines = patient.answerScenarios[index].scenarioLines;

            isAskingQuestions = true;
        }
        void ActivateQuestionButtonGroup() {
            questionBtns.ForEach(i => i.interactable = true);
            questionBtnsParent.SetActive(true);
        }

        void DeactivateQuestionButtonGroup() {
            questionBtns.ForEach(i => i.interactable = false);
            questionBtnsParent.SetActive(false);
        }
    }
}