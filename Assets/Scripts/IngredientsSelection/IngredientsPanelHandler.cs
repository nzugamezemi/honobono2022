using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientsPanelHandler : MonoBehaviour {

    public List<ItemIconHandler> contents = new List<ItemIconHandler>();
    public List<Vector2> defaultPositions = new List<Vector2>();

    private void Start() {
        defaultPositions = new List<Vector2>();
        for (int i = 0; i < contents.Count; i++) {
            defaultPositions.Add(contents[i].transform.position);
        }
    }
}