using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class IngredientsSelectionManager : MonoBehaviour
{

    [SerializeField] PlayerInputsReceiver input;

    [SerializeField] DataAccessUnit DAU;

    [SerializeField]
    ItemRecipeDatabase itemRecipeDatabase;

    [Header("全材料管理")]
    [SerializeField] IngredientsPanelHandler ingredientsPanelHandler;

    [SerializeField] Button startFormulateBtn;
    bool selectedStartFormulateBtn = false;
    public void SetSelectedStartFormulateBtnState(bool newState) => selectedStartFormulateBtn = newState;

    [SerializeField] Button backCollectBtn;

    [System.Serializable]
    class SlotData
    {
        public int slotIndex { get; private set; }
        public void SetIndex(int val) => slotIndex = val;
        public ItemIconHandler itemIconHandler;
        public int selectedItemIndex;

        SlotData()
        {
            slotIndex = -1;
        }

        public void Initialize(int slotIndex = -1)
        {
            this.slotIndex = slotIndex;
            selectedItemIndex = -1;
            itemIconHandler.ApplyItemData(null);
        }

        public void ResetSlot()
        {
            selectedItemIndex = -1;
            itemIconHandler.ApplyItemData(null);
        }
    }

    [Header("選択材料管理")]
    [SerializeField] List<SlotData> slotList = new List<SlotData>();
    int lastSelectingSlotIndex = -1;
    int currentSelectingSlotIndex = -1;
    [SerializeField] RectTransform slotHighlighterRectTrns;
    List<Item> selectedItems = new List<Item>();
    [SerializeField] RectTransform inventoryHighlighter1RectTrns;
    [SerializeField] RectTransform inventoryHighlighter2RectTrns;

    [Header("薬剤種類管理")]
    [SerializeField] List<Button> medicineTypeBtnGroup = new List<Button>();
    ItemRecipeDatabase.MedicineType selectedMedicineType = ItemRecipeDatabase.MedicineType.None;
    [SerializeField] RectTransform medicineTypeHighlighterRectTrns;

    [Header("見た目制御")]
    [SerializeField] SelectedContentsContainerHandler selectedContentsContainerHandler;

    void Start() => Initialize();

    void Initialize()
    {
        // インベントリを作成
        /*inventoryPanelHandler.CreateInventoryList(inventoryManager);
        // インベントリ内容セットアップ
        var _contents = inventoryPanelHandler.GetContentList();
        for (int i = 0; i < _contents.Count; i++) {
            int _index = i;
            var targetContent = _contents[_index];
            targetContent.ActivateVirtualButton();
            targetContent.AddClickListener(() => {
                Item _itemData = targetContent.GetItemData();
                OnSelectIngredient(_itemData);
            });
        }
        _contents[0].vBtn.Select();*/

        // 戻る・進むボタンのセットアップ
        startFormulateBtn.interactable = false;
        startFormulateBtn.onClick.RemoveAllListeners();
        startFormulateBtn.onClick.AddListener(OnClickStartFormulateButton);
        backCollectBtn.interactable = true;
        backCollectBtn.onClick.RemoveAllListeners();
        backCollectBtn.onClick.AddListener(OnClickBackCollectButton);

        var _contents = ingredientsPanelHandler.contents;
        // 材料をすべてセット & インベントリのデータを転写
        List<InventoryManager.SingleInventoryData> _inv = new List<InventoryManager.SingleInventoryData>();
        _inv.AddRange(DAU.data.playerInventory);

        if (_contents != null && _contents.Count() > 0)
        {
            /*for (int i = 0; i < _inv.Count; i++)
            {
                Item _itemData = _inv[i].itemData;
                ItemIconHandler targetContent = _contents[i];
                targetContent.ApplyItemData(_itemData);
                targetContent.ActivateVirtualButton();
                targetContent.AddClickListener(() =>
                {
                    // Item _itemData = targetContent.GetItemData();
                    OnSelectIngredient(_itemData);
                });
            }*/

            bool settedSelected = false;

            for (int i = 0; i < _contents.Count; i++)
            {
                int _index = i;
                Item _itemData = itemRecipeDatabase.AllIngredients[_index];
                ItemIconHandler targetContent = _contents[_index];
                targetContent.ApplyItemData(_itemData);

                // インベントリに存在する
                if (_inv.Any(i => i.itemData == _itemData))
                {
                    targetContent.ActivateVirtualButton();
                    if (targetContent.GetItemData()._icon_highlighted)
                    {
                        targetContent.vBtn.transition = Selectable.Transition.SpriteSwap;
                        var tmpSpriteState = targetContent.vBtn.spriteState;
                        tmpSpriteState.selectedSprite = targetContent.GetItemData()._icon_highlighted;
                        targetContent.vBtn.spriteState = tmpSpriteState;
                    }
                    targetContent.AddClickListener(() =>
                    {
                        OnSelectIngredient(_itemData);
                    });

                    if (!settedSelected)
                    {
                        settedSelected = true;
                        targetContent.vBtn.Select();
                    }
                }
            }

            if (_inv == null || _inv.Count == 0)
            {
                backCollectBtn.Select();
            }
        }
        else
        {
            Debug.Log("素材パネルデータがありません");
        }


        // スロットセットアップ
        SetupSlots();

        currentSelectingSlotIndex = 0;
        lastSelectingSlotIndex = 0;

        for (int i = 0; i < medicineTypeBtnGroup.Count; i++)
        {
            int _index = i;
            Button tBtn = medicineTypeBtnGroup[_index];
            tBtn.onClick.AddListener(() => OnMedicineTypeChanged(_index));
        }

        selectedMedicineType = ItemRecipeDatabase.MedicineType.Powdered;
    }

    void SetupSlots()
    {
        if (slotList != null && slotList.Count > 0)
        {
            for (int i = 0; i < slotList.Count; i++)
            {
                var _slotIndex = i;
                SlotData _slotData = slotList[_slotIndex];
                _slotData.Initialize(_slotIndex);
            }
        }
    }

    void Update()
    {
        CheckInputs();

        // slotHighlighterRectTrns.position = Vector3.Lerp(slotHighlighterRectTrns.position, slotList[currentSelectingSlotIndex].itemIconHandler.gameObject.transform.position, Time.deltaTime * 15f);
    }

    void CheckInputs()
    {
        if (input.cancel)
        {
            OnCancelButton();
            // reset
            input.CancelInput(false);
        }
    }

    /// <summary>
    /// 調合開始ボタン関数
    /// </summary>
    void OnClickStartFormulateButton()
    {
        // 選んだ情報をデータ共有
        // 成功判定
        {
            List<Item> _ingredients = new List<Item>();
            slotList.ForEach(i => { _ingredients.Add(i.itemIconHandler.GetItemData()); });
            // Recipe _targetRecipe = itemRecipeDatabase.GetRecipeDataFromIngredients(_ingredients);
            // DAU.data.targetRecipe = _targetRecipe;
            // DAU.data.targetMedicineType = medicineType;

            DAU.data.SetSelectedMedicineType(selectedMedicineType);

            // レシピ取得
            Recipe tRecipe = itemRecipeDatabase.GetRecipeDataFromIngredients(_ingredients);

            // 作成される薬データ転送
            if (tRecipe != null)
            {
                // タイプが一致しているか
                bool typeMatch = tRecipe.medicineType == selectedMedicineType;
                if (typeMatch)
                {
                    DAU.data.SetCreatedMedicineData(tRecipe.result);
                }
                else
                {
                    // レシピと違えばnullを格納
                    DAU.data.SetCreatedMedicineData(null);
                    Debug.Log("レシピに合う薬剤がありませんでした");
                }
            }
            else
            {
                // レシピと違えばnullを格納
                DAU.data.SetCreatedMedicineData(null);
                Debug.Log("レシピに合う薬剤がありませんでした");
            }

            // インベントリからアイテムを抜く
            _ingredients.ForEach(i =>
            {
                DAU.data.playerInventory.RemoveAll(i2 => i2.itemData == i);
            });

            DAU.data.ApplyFormulatedMedcineResultState();
        }

        UnityEngine.SceneManagement.SceneManager.LoadScene("ResultScene");
    }

    /// <summary>
    /// 採取に戻るボタン関数
    /// </summary>
    void OnClickBackCollectButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("PrototypeScene");
    }

    /// <summary>
    /// 材料を選択したときの挙動
    /// </summary>
    public void OnSelectIngredient(Item itemData)
    {
        if (selectedItems.Contains(itemData))
        {
            bool removeFromFirst = selectedItems.IndexOf(itemData) == 0;

            var _contents = ingredientsPanelHandler.contents;
            for (int i = 0; i < _contents.Count; i++)
            {
                int _index = i;
                if (_contents[_index].GetItemData() == itemData)
                {
                    // Debug.Log(_contents[_index].transform.position);
                    StartCoroutine(MoveButton(_contents[_index], ingredientsPanelHandler.defaultPositions[_index], () => _contents[_index].vBtn.Select()));
                    break;
                }
            }

            if (removeFromFirst && selectedItems.Count > 1)
            {
                ItemIconHandler targetBtn = null;
                foreach (var i in _contents)
                {
                    if (i.GetItemData() == selectedItems[1])
                    {
                        targetBtn = i;
                    }
                }
                StartCoroutine(MoveButton(targetBtn, selectedContentsContainerHandler.positions[0].transform.position));
            }

            selectedItems.Remove(itemData);
        }
        else
        {
            if (selectedItems.Count < slotList.Count)
            {
                selectedItems.Add(itemData);

                var _contents = ingredientsPanelHandler.contents;
                for (int i = 0; i < _contents.Count; i++)
                {
                    int _index = i;
                    if (_contents[_index].GetItemData() == itemData)
                    {
                        // Debug.Log(_contents[_index].transform.position);
                        StartCoroutine(MoveButton(_contents[_index], selectedContentsContainerHandler.positions[Mathf.Clamp(selectedItems.Count - 1, 0, 1)].transform.position));

                        if (_index < _contents.Count - 1)
                        {
                            _contents[_index + 1].vBtn.Select();
                        }
                        else if (_index == _contents.Count - 1)
                        {
                            if (selectedItems.Contains(_contents[0].GetItemData()))
                            {
                                _contents[1].vBtn.Select();
                            }
                            else
                            {
                                _contents[0].vBtn.Select();
                            }
                        }
                        break;
                    }
                }
            }
        }

        // スロット更新
        RefreshSlots();

        currentSelectingSlotIndex = Mathf.Clamp(selectedItems.Count, 0, 1);

        /*SlotData targetSlotData = slotList[currentSelectingSlotIndex];

        // スロットにデータ適応
        ApplyItemDataToSlot(itemData, targetSlotData);

        // 選択スロット繰り上げ
        if (currentSelectingSlotIndex < slotList.Count - 1) {
            currentSelectingSlotIndex++;
        }*/

        OnChangedSlotIndex();
    }

    void RefreshSlots()
    {
        inventoryHighlighter1RectTrns.gameObject.SetActive(false);
        inventoryHighlighter2RectTrns.gameObject.SetActive(false);

        // スロットデータリフレッシュ
        for (int i = 0; i < slotList.Count; i++)
        {
            SlotData targetSlotData = slotList[i];
            targetSlotData.ResetSlot();

            if (i < selectedItems.Count)
            {
                // スロットにデータ適応
                ApplyItemDataToSlot(selectedItems[i], targetSlotData);

                /*var invList = inventoryPanelHandler.GetContentList();
                foreach (var content in invList) {
                    if (content.GetItemData() == selectedItems[i]) {
                        if (i == 0) {
                            inventoryHighlighter1RectTrns.position = content.gameObject.transform.position;
                            inventoryHighlighter1RectTrns.gameObject.SetActive(true);
                        } else if (i == 1) {
                            inventoryHighlighter2RectTrns.position = content.gameObject.transform.position;
                            inventoryHighlighter2RectTrns.gameObject.SetActive(true);
                        }
                    }
                }*/
            }
        }
    }

    void OnChangedSlotIndex()
    {
        if (slotList.TrueForAll(delegate (SlotData s) { return s.selectedItemIndex != -1; }))
        {
            // 次へ、戻るのボタンアクティブ
            startFormulateBtn.interactable = true;
        }
        else
        {
            // 次へのボタン非アクティブ
            if (startFormulateBtn.interactable) startFormulateBtn.interactable = false;

            if (selectedStartFormulateBtn)
            {
                ingredientsPanelHandler.contents[0].vBtn.Select();
                selectedStartFormulateBtn = false;
            }
        }
    }

    void ApplyItemDataToSlot(Item newItemData, SlotData targetSlotData)
    {
        // アイコンハンドラにアイテムデータ転送
        targetSlotData.itemIconHandler.ApplyItemData(newItemData);
        // アイテムインデックスを登録
        targetSlotData.selectedItemIndex = itemRecipeDatabase.GetIngredientIndexFromItemData(newItemData);
    }

    public void OnCancelButton()
    {
        // 材料選択を一つ戻る
        if (currentSelectingSlotIndex >= 0)
        {
            slotList.ForEach(i => i.ResetSlot());
            currentSelectingSlotIndex--;
            selectedItems.Clear();
            RefreshSlots();

            OnChangedSlotIndex();
        }
    }

    void OnMedicineTypeChanged(int value)
    {
        selectedMedicineType = (ItemRecipeDatabase.MedicineType)System.Enum.ToObject(typeof(ItemRecipeDatabase.MedicineType), value);

        medicineTypeHighlighterRectTrns.position = medicineTypeBtnGroup[value].transform.position;
    }

    IEnumerator MoveButton(ItemIconHandler targetIcon, Vector2 end, System.Action callback = null)
    {
        targetIcon.vBtn.interactable = false;

        while (true)
        {
            float diffDis = Vector2.Distance(targetIcon.transform.position, end);
            targetIcon.transform.position = Vector2.MoveTowards(targetIcon.transform.position, end, diffDis * Time.deltaTime * 50f);

            if (diffDis <= 0.1f)
            {
                break;
            }

            yield return null;
        }

        targetIcon.transform.position = end;
        targetIcon.vBtn.interactable = true;

        callback?.Invoke();

        yield break;
    }
}