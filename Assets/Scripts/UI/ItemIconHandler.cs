using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemIconHandler : MonoBehaviour {

    [SerializeField] private Item m_itemData;

    public void ApplyItemData(Item item) {
        m_itemData = item;

        UpdateIconImage(item);
    }
    public Item GetItemData() => m_itemData;

    [SerializeField] private Image _iconImg;   // アイコン用イメージ
    [SerializeField] private Sprite _defaultIconImg;   // デフォルトアイコン用スプライト
    [SerializeField] private TMPro.TextMeshProUGUI _numberText;   // 個数表示用テキスト
    [SerializeField] private Button _virtualBtn;   // 判定用仮想ボタン
    public Button vBtn => _virtualBtn;

    private void Start() {
        UpdateIconImage(m_itemData);
    }

    /// <summary>
    /// アイテムの所持数を表示
    /// </summary>
    /// <param name="number"></param>
    public void DisplayNumberText(int number) {
        // 個数自体はインベントリ側で管理
        if (_numberText) {
            _numberText.text = number.ToString();
            _numberText.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// アイコンのクリックを可能に
    /// </summary>
    /// <returns></returns>
    public Button ActivateVirtualButton() {
        if (_virtualBtn) {
            _virtualBtn.interactable = true;
            _virtualBtn.enabled = true;
            return _virtualBtn;
        } else {
            return null;
        }
    }

    /// <summary>
    /// アイコンクリック時のコールバック登録
    /// </summary>
    /// <param name="action"></param>
    public void AddClickListener(System.Action action) {
        if (_virtualBtn) {
            _virtualBtn.onClick.AddListener(() => action?.Invoke());
        }
    }

    public void UpdateIconImage(Item itemData) {
        Sprite targetIcon = itemData != null ? itemData._icon : _defaultIconImg;

        if (_iconImg) _iconImg.sprite = targetIcon;
        _virtualBtn.image.sprite = targetIcon;
    }
}