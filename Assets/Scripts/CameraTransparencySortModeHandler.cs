using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransparencySortModeHandler : MonoBehaviour {

    void Start() {
        // カメラのソートモードを変更
        Camera.main.transparencySortMode = TransparencySortMode.Orthographic;
    }

}