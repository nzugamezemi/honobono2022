using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "アイテム情報")]
public class Item : ScriptableObject {
    public int _id;
    public string _name;
    public string _desc;
    public Sprite _icon;
    public Sprite _icon_highlighted;
}

#if UNITY_EDITOR
[CustomEditor(typeof(Item))]
class ItemEditor : Editor {

    // インスペクタ拡張クラス
    public override void OnInspectorGUI() {
        // base.OnInspectorGUI();

        Item data = target as Item;

        EditorGUILayout.Space(10);

        EditorGUILayout.LabelField("[ アイテム情報設定 ]", new GUIStyle() { fontSize = 14, fontStyle = FontStyle.Bold });

        EditorGUILayout.Space(20);

        EditorGUI.BeginChangeCheck();

        data._id = EditorGUILayout.IntField("アイテムID", data._id);
        data._name = EditorGUILayout.TextField("アイテム名", data._name);
        EditorGUILayout.LabelField("アイテム説明");
        data._desc = EditorGUILayout.TextArea(data._desc);
        data._icon = EditorGUILayout.ObjectField("アイコン画像", data._icon, typeof(Sprite), false) as Sprite;
        data._icon_highlighted = EditorGUILayout.ObjectField("アイコン画像（ハイライト状態）", data._icon_highlighted, typeof(Sprite), false) as Sprite;

        if (EditorGUI.EndChangeCheck()) {
            EditorUtility.SetDirty(data);   // Dirtyフラグを立ててUnity終了時に.assetに書き出す
        }
    }
}
#endif