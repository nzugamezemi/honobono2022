using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class DataHolder : ScriptableObject {

	// 作る薬のレシピデータ
	Recipe m_TargetRecipe;
	public Recipe targetRecipe;

	// 作る薬タイプ
	public ItemRecipeDatabase.MedicineType targetMedicineType => targetRecipe.medicineType;

	// 選択した材料から出来上がる薬
	Item createdMedicine = null;
	public void SetCreatedMedicineData(Item newItemData) => createdMedicine = newItemData;
	/// <summary>
	/// 薬が作れているか
	/// </summary>
	public bool ExistMedicine => createdMedicine != null;
	public Item GetCreatedMedicine() => createdMedicine;

	// 選択した薬タイプ
	ItemRecipeDatabase.MedicineType selectedMedicineType = ItemRecipeDatabase.MedicineType.None;
	public void SetSelectedMedicineType(ItemRecipeDatabase.MedicineType newVal) => selectedMedicineType = newVal;

	// 患者のデータ
	Diagnose.Patient m_Patient;
	public Diagnose.Patient patient;

	// 診断してあるか（診断モード中か）
	bool m_IgnoreDiagnose = false;
	public bool ignoreDiagnose;

    // インベントリデータ
    List<InventoryManager.SingleInventoryData> m_PlayerInventory;
	public List<InventoryManager.SingleInventoryData> playerInventory;

	// 調合に成功したか
	public enum FormulatedMedicineResultState {
		None=-1,
		Success,
		Failed
    }
	FormulatedMedicineResultState m_FormulationResultState = FormulatedMedicineResultState.None;
	public FormulatedMedicineResultState formulationResultState => m_FormulationResultState;

	/// <summary>
	/// 調合が成功しているか判定
	/// </summary>
	/// <returns></returns>
	public FormulatedMedicineResultState ApplyFormulatedMedcineResultState() {
		if (m_FormulationResultState != FormulatedMedicineResultState.None) {
			return m_FormulationResultState;
		}

		FormulatedMedicineResultState _state = FormulatedMedicineResultState.None;

		if (patient == null) {
			Debug.LogError("患者データがありません");
			_state = FormulatedMedicineResultState.None;
			m_FormulationResultState = _state;
			return _state;
		}

		if (targetRecipe == null) {
			Debug.LogError("レシピデータが存在しません");
			_state = FormulatedMedicineResultState.None;
			m_FormulationResultState = _state;
			return _state;
		}

		if (targetMedicineType == ItemRecipeDatabase.MedicineType.None) {
			Debug.LogError("薬剤タイプが設定されていません");
			_state = FormulatedMedicineResultState.None;
			m_FormulationResultState = _state;
			return _state;
		}

        // 薬が患者の症状と合っているか
        if (ExistMedicine) {
			if (patient.medicine == GetCreatedMedicine()) {
				Debug.Log("薬が患者の症状と合っている");
				_state = FormulatedMedicineResultState.Success;
			} else {
				Debug.Log("薬が患者の症状と合っていない");
				_state = FormulatedMedicineResultState.Failed;
			}
        } else {
			Debug.Log("薬の作成に失敗");
			_state = FormulatedMedicineResultState.Failed;
		}

		m_FormulationResultState = _state;
		return _state;
	}

    void OnEnable() {
		ResetAll();

		targetRecipe = m_TargetRecipe;
		patient = m_Patient;
		ignoreDiagnose = m_IgnoreDiagnose;
        playerInventory = m_PlayerInventory;
	}

	public void ResetAll() {
		m_TargetRecipe = null;
		m_Patient = null;
		m_IgnoreDiagnose = false;
		if (m_PlayerInventory == null || m_PlayerInventory.Count == 0) m_PlayerInventory = new List<InventoryManager.SingleInventoryData>();
		m_FormulationResultState = FormulatedMedicineResultState.None;
		createdMedicine = null;
		selectedMedicineType = ItemRecipeDatabase.MedicineType.None;

		targetRecipe = m_TargetRecipe;
		patient = m_Patient;
		ignoreDiagnose = m_IgnoreDiagnose;
        playerInventory = m_PlayerInventory;
	}
}