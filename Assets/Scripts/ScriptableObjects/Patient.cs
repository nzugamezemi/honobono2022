using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Diagnose {
    [CreateAssetMenu(menuName = "患者情報")]
    public class Patient : ScriptableObject {

        [System.Serializable]
        public class ScenarioLine {
            public bool right;
            public string line;

            public ScenarioLine(string line) {
                this.line = line;
            }
        }

        [System.Serializable]
        public class ScenarioLineList {
            public ScenarioLine[] scenarioLines;

            public ScenarioLineList(ScenarioLine[] scenarioLines) {
                this.scenarioLines = scenarioLines;
            }
        }

        [Header("患者名（今のところ無意味）")]
        public string _name="test_patient_001";

        [Header("患者画像（病気）")]
        public Sprite _diseaseSprite;
        [Header("患者画像（成功時）")]
        public Sprite _goodSprite;
        [Header("患者画像（失敗時）")]
        public Sprite _badSprite;

        [Header("病名（今のところ無意味？）")]
        public string diseaseName = "test_disease";

        [Space(5)]

        [Header("薬（調合成功判定用アイテムデータ）")]
        public Item medicine;

        [Space(5)]

        [Header("挨拶シナリオ")]
        // テキストデータ
        public ScenarioLine[] enterGreetingScenarios;

        [Space(5)]

        [Header("病気の質問リスト")]
        public string[] questions;

        [Header("病気質問の回答シナリオリスト")]
        public ScenarioLineList[] answerScenarios;

        [Space(5)]

        [Header("薬受け取りシナリオ（成功時）")]
        public ScenarioLine[] receiveSuccessedMedicineScenarios;

        [Header("薬受け取りシナリオ（失敗時）")]
        public ScenarioLine[] receiveFailedMedicineScenarios;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Patient))]
    class PatientEditor : Editor {

        // インスペクタ拡張クラス
        public override void OnInspectorGUI() {

            Patient data = target as Patient;

            EditorGUI.BeginChangeCheck();

            base.OnInspectorGUI();

            if (EditorGUI.EndChangeCheck()) {
                EditorUtility.SetDirty(data);   // Dirtyフラグを立ててUnity終了時に.assetに書き出す
                // AssetDatabase.SaveAssets();   // だが一応保存
            }
        }
    }
#endif
}