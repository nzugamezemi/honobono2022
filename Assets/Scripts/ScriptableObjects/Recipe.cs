using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "レシピ")]
public class Recipe : ScriptableObject {

    [Header("調合材料"), SerializeField]
    List<Item> m_ingredients;
    [Header("調合タイプ"), SerializeField]
    ItemRecipeDatabase.MedicineType m_medicineType;
    [Header("調合結果"), SerializeField]
    Item m_result;

    public List<Item> ingredients => m_ingredients;
    public Item result => m_result;
    public ItemRecipeDatabase.MedicineType medicineType => m_medicineType;

    public List<Item> GetSortedIngredients() {
        if (m_ingredients == null || m_ingredients.Count == 0) return null;

        List<Item> _list = new List<Item>(m_ingredients);
        _list.Sort((a, b) => string.Compare(a._name, b._name));
        return _list;
    }
}