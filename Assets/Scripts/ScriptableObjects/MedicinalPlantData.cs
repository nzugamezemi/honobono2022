using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "薬草オブジェクトデータ情報")]
public class MedicinalPlantData : ScriptableObject {

    [Header("採取アイテムデータ")]
    public Item item;

    [Header("成長画像データ")]
    public Sprite firstStageSprite;
    public Sprite secondStageSprite;
    public Sprite thirdStageSprite;

    [Header("完成段階")]
    public MedicinalPlantCollectPointHandler.GrowthStage maxGrothStage = MedicinalPlantCollectPointHandler.GrowthStage.Third;

    [Header("成長の所要時間（s）")]
    public float timeSpanStillToFirst = 1f;
    public float timeSpanFirstToSecond = 1f;
    public float timeSpanSecondToThird = 1f;
}

#if UNITY_EDITOR
[CustomEditor(typeof(MedicinalPlantData))]
class MedicinalPlantDataEditor : Editor {

    // インスペクタ拡張クラス
    public override void OnInspectorGUI() {
        MedicinalPlantData data = target as MedicinalPlantData;

        GUILayout.Space(15);

        EditorGUILayout.LabelField($"【アイテム名 : {(data ? data.item._name : "Null")}】");

        GUILayout.Space(15);

        base.OnInspectorGUI();

        if (EditorGUI.EndChangeCheck()) {
            EditorUtility.SetDirty(data);   // Dirtyフラグを立ててUnity終了時に.assetに書き出す
        }
    }
}
#endif