using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class FormulateExeManager : SingletonMonoBehaviour<FormulateExeManager> {

    public PlayerInputsReceiver input;

    [SerializeField] TextMeshProUGUI _uiText;

    [SerializeField] TextMeshProUGUI _pUiText1;
    [SerializeField] TextMeshProUGUI _pUiText2;
    [SerializeField] TextMeshProUGUI _pUiText3;

    // ログ表示用
    [SerializeField] TextMeshProUGUI _logUiText;
    string _log = "Log";
    public void AddLogMsg(string str) {
        _log += "\n" + str;
    }

    [SerializeField] Slider timeBarSlider1;
    [SerializeField] Slider timeBarSlider2;

    private bool finish = false;
    public bool IsFinished() => finish;

    private bool runningExitProcess = false;

    int phaseFailedCount = 0;

    [SerializeField] Animator playerAnim;    // プレイヤーアニメータ
    [SerializeField] Animator effAnim;    // エフェクトアニメータ
    [SerializeField] ParticleSystem kirakiraEff;
    [SerializeField] ParticleSystem guruguruEff;

    // カメラ動かす用
    [SerializeField]
    Animator camAnimator;
    bool moving = false;

    // アクション区別用
    PlayerFormulateActionDiscriminator.Rotate stickRotateAction;
    PlayerFormulateActionDiscriminator.UpDown stickUpDownAction;
    PlayerFormulateActionDiscriminator.ButtonPressing buttonPressingAction;
    PlayerFormulateActionDiscriminator.DPadPressing dPadPressingAction;

    public enum FormulateActionType {
        Rotate = 0,
        UpDown,
        ButtonMash,
        DPadPress,
        ButtonLongPress,
        ButtonTiming
    }

    private ItemRecipeDatabase.MedicineType targetMedicineType;
    public void SetTargetMedicineType(ItemRecipeDatabase.MedicineType newType) {
        targetMedicineType = newType;
    }
    public ItemRecipeDatabase.MedicineType GetTargetMedicineType() => targetMedicineType;

    public delegate bool FormulateActionUpdate();
    FormulateActionUpdate formulateActionUpdateMethod;
    public List<FormulateActionUpdate> formulateActionUpdateMethodList = new List<FormulateActionUpdate>();
    void CreateFormulateActionUpdateMethodList() {

        /*
        Rotate,
        UpDown,
        ButtonMash,
        DPadPress,
        ButtonLongPress,
        ButtonTiming*/

        formulateActionUpdateMethodList.Clear();
        formulateActionUpdateMethodList = new List<FormulateActionUpdate>() {
            stickRotateAction.UpdateState,
            stickUpDownAction.UpdateState,
            buttonPressingAction.UpdateState,
            dPadPressingAction.UpdateState,
            buttonPressingAction.UpdateState,
            buttonPressingAction.UpdateState
        };
    }

    // FormulationDatabaseのMedicineType参照
    /*enum MedicineType {
        None = -1,
        Powdered,   // 粉
        Syrup,   // シロップ
        Tablet,   // 錠
        Cream,   // クリーム
        Aroma_Solid,   // アロマ固体
        Aroma_Liquid   // アロマ液体
    }*/

    // ↑順番を同じに
    List<List<FormulateActionType>> FormulationOrderList = new List<List<FormulateActionType>>() {
        new List<FormulateActionType>() {   // 粉薬
            FormulateActionType.UpDown,
            FormulateActionType.Rotate,
            FormulateActionType.DPadPress
        },
        new List<FormulateActionType>() {   // シロップ剤
            FormulateActionType.ButtonMash,
            FormulateActionType.Rotate,
            FormulateActionType.ButtonLongPress
        },
        new List<FormulateActionType>() {   // 錠剤
            FormulateActionType.UpDown,
            FormulateActionType.Rotate,
            FormulateActionType.ButtonTiming
        },
        new List<FormulateActionType>() {   // クリーム
            FormulateActionType.UpDown,
            FormulateActionType.Rotate,
            FormulateActionType.ButtonLongPress
        },
        new List<FormulateActionType>() {   // アロマ（固体）
            FormulateActionType.UpDown,
            FormulateActionType.Rotate,
            FormulateActionType.ButtonTiming
        },
        new List<FormulateActionType>() {   // アロマ（液体）
            FormulateActionType.ButtonMash,
            FormulateActionType.Rotate,
            FormulateActionType.ButtonLongPress
        }
    };

    List<FormulateActionType> orderedFormulateActionsList = new List<FormulateActionType>();
    int currentFormulateActionIndex;
    FormulateActionType currentFormulateActionPhase;

    // メインループ用
    UnityEvent mainUpdate = new UnityEvent();
    MainUpdateMethods mainStateHandler;

    public class MainUpdateMethods {

        public FormulateExeManager root;

        const int LIMIT_TIME = 30;

        public MainUpdateMethods(FormulateExeManager root) {
            this.root = root;

            Initialize();
        }

        private bool complete = false;
        public bool IsCompleted() => complete;
        public bool OverrideCompleteState(bool newState) => complete = newState;

        private bool success = false;
        private int successCount = 0;
        public bool UpdateSuccessState() {
            if (success) {
                success = false;
                successCount++;
                root.AddLogMsg("<color=#f54254>成功</color>");
                // root.effAnim.CrossFadeInFixedTime("Success", 0.1f);
                root.kirakiraEff.Play();
                return true;
            }

            return success;
        }

        private bool failure = false;
        private int failureCount = 0;
        public bool UpdateFailureState() {
            if (failure) {
                failure = false;
                failureCount++;
                root.AddLogMsg("<color=#4542f5>失敗</color>");
                // root.effAnim.CrossFadeInFixedTime("Failure", 0.1f);
                root.guruguruEff.Play();
                return true;
            }

            return failure;
        }

        public bool phaseFailed = false;

        private float tmpElapsedTime = 0f;   // 経過時間
        private float totalElapsedTime = 0f;   // 総経過時間

        private int tmpActionCount = 0;
        private int diffActionCount = 0;
        private int totalActionCount = 0;


        private float tmpActionValue = 0;
        private float totalActionValue = 0;

        public void Initialize() {
            complete = false;

            success = false;
            successCount = 0;
            failure = false;
            failureCount = 0;

            phaseFailed = false;

            tmpElapsedTime = 0;
            totalElapsedTime = 0;

            tmpActionCount = 0;
            diffActionCount = 0;
            totalActionCount = 0;

            tmpActionValue = 0;
            totalActionValue = 0;
        }

        public void MutualUpdate() {
            float deltaTime = Time.deltaTime;
            tmpElapsedTime += deltaTime;
            totalElapsedTime += deltaTime;

            UpdateSuccessState();
            UpdateFailureState();

            root.timeBarSlider1.value = totalElapsedTime / LIMIT_TIME;

            // 制限時間を超える・失敗が10回になったら終わり
            if (totalElapsedTime > LIMIT_TIME || failureCount >= 10) {
                phaseFailed = true;
                root.phaseFailedCount++;
                OverrideCompleteState(true);
            }
        }

        public void StickRotate() {
            // Debug.Log("stick rot");

            string str =
            $"調合シーン (実行中) [Type:{root.targetMedicineType.ToString()}] \n\n" +
            $"スティック回し";

            root._uiText.text = str;

            root.timeBarSlider2.value = tmpElapsedTime / 1;

            totalActionCount = root.stickRotateAction.totalRotNum;

            if (tmpElapsedTime >= 1f) {
                diffActionCount = totalActionCount - tmpActionCount;
                tmpActionCount = totalActionCount;

                if (diffActionCount == 1) {
                    success = true;
                } else {
                    failure = true;
                }

                tmpElapsedTime = 0;
            }
        }
        public void StickUpDown() {
            Debug.Log("stick ud");

            string str =
            $"調合シーン (実行中) [Type:{root.targetMedicineType.ToString()}] \n\n" +
            $"スティック上下";

            root._uiText.text = str;
        }
        public void ButtonMash() {
            // Debug.Log("btn m");

            string str =
            $"調合シーン (実行中) [Type:{root.targetMedicineType.ToString()}] \n\n" +
            $"ボタン連打";

            root._uiText.text = str;

            root.timeBarSlider2.value = tmpElapsedTime / 1;

            totalActionCount = root.buttonPressingAction.repeatedCount;

            if (tmpElapsedTime >= 1f) {
                diffActionCount = totalActionCount - tmpActionCount;
                tmpActionCount = totalActionCount;

                if (diffActionCount == 2 || diffActionCount == 3) {
                    success = true;
                } else {
                    failure = true;
                }

                tmpElapsedTime = 0;
            }
        }
        public void DPadPress() {
            Debug.Log("dP prs");

            string str =
            $"調合シーン (実行中) [Type:{root.targetMedicineType.ToString()}] \n\n" +
            $"十字キーを順番に入力";

            root._uiText.text = str;
        }
        public void ButtonLongPress() {
            Debug.Log("btn lp");

            string str =
            $"調合シーン (実行中) [Type:{root.targetMedicineType.ToString()}] \n\n" +
            $"ボタン長押し";

            root._uiText.text = str;

            bool btnState = root.buttonPressingAction.btnState;
            if (btnState == true) {
                tmpActionValue += Time.deltaTime * 0.1f;
            }

            root.timeBarSlider2.value = tmpActionValue / 1;

            if (tmpActionValue >= 1f) {
                OverrideCompleteState(true);
            }
        }
        public void ButtonTiming() {
            Debug.Log("btn tim");

            string str =
            $"調合シーン (実行中) [Type:{root.targetMedicineType.ToString()}] \n\n" +
            $"ボタンをタイミング良く押す";

            root._uiText.text = str;
        }
    }

    [SerializeField]
    RectTransform stickArea, stickPad;

    private void Start() {
        stickRotateAction = new PlayerFormulateActionDiscriminator.Rotate();
        stickUpDownAction = new PlayerFormulateActionDiscriminator.UpDown();
        buttonPressingAction = new PlayerFormulateActionDiscriminator.ButtonPressing();
        dPadPressingAction = new PlayerFormulateActionDiscriminator.DPadPressing();

        CreateFormulateActionUpdateMethodList();

        mainStateHandler = new MainUpdateMethods(this);

        ItemRecipeDatabase.MedicineType _targetType = ItemRecipeDatabase.MedicineType.Syrup;
        SetFormulationOrder(_targetType);

        _pUiText1.text = orderedFormulateActionsList[0].ToString();
        _pUiText2.text = orderedFormulateActionsList[1].ToString();
        _pUiText3.text = orderedFormulateActionsList[2].ToString();

        currentFormulateActionIndex = 0;
        SetFormulateActionType(currentFormulateActionIndex);
        ApplyAnimationParam();

        moving = false;
        runningExitProcess = false;
    }

    private void Update() {
        if (moving) {
            float nT = camAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            if (nT >= 1f) moving = false;
            return;
        }

        if (finish) {
            // 終了時のUpdate()
            FinishedUpdate();
            return;
        }

        _logUiText.text = _log;

        if (mainStateHandler.IsCompleted()) {
            GoNextFormulationPhase();
            _log = "Log";
            return;
        }

        // 入力判定の更新
        formulateActionUpdateMethod?.Invoke();

        // 現在の工程のメインループ
        mainUpdate?.Invoke();
        mainStateHandler.MutualUpdate();

        Vector2 stickInput = input.rawMove;
        stickPad.anchoredPosition = stickInput * (stickArea.rect.width / 2);

        if (UnityEngine.InputSystem.Keyboard.current.spaceKey.wasPressedThisFrame) {
            mainStateHandler.OverrideCompleteState(true);
        }
    }

    public void GoNextFormulationPhase() {
        StartMove();

        if (!CheckFinish()) {
            currentFormulateActionIndex++;
            currentFormulateActionPhase = SetFormulateActionType(currentFormulateActionIndex);

            SetUpdateMethods(currentFormulateActionPhase);

            // アニメーション変更
            ApplyAnimationParam();
        } else {
            FinishFormulating();
        }
    }

    FormulateActionType SetFormulateActionType(int index) => orderedFormulateActionsList[index];

    bool CheckFinish() {
        if (currentFormulateActionIndex < orderedFormulateActionsList.Count - 1) {
            return false;
        } else {
            return true;
        }
    }

    void StartMove() {
        moving = true;

        camAnimator.CrossFadeInFixedTime("CameraMove", 0);
    }

    void ApplyAnimationParam() {
        if (playerAnim) {
            string _stateName = GetAnimationName(currentFormulateActionPhase);
            playerAnim.CrossFadeInFixedTime(_stateName, 0.1f);
        }
    }

    string GetAnimationName(FormulateActionType type) {
        string _r = "Idle";
        switch (type) {
            case FormulateActionType.ButtonMash:
                _r = "Mash";
                break;
            case FormulateActionType.Rotate:
                _r = "Rotate";
                break;
            case FormulateActionType.ButtonLongPress:
                // _r = "Rotate";
                break;
        }
        return _r;
    }

    // 薬の種類から工程の順番を指定
    public void SetFormulationOrder(ItemRecipeDatabase.MedicineType medicineType) {
        SetTargetMedicineType(medicineType);

        orderedFormulateActionsList.Clear();
        orderedFormulateActionsList.AddRange(FormulationOrderList[(int)medicineType]);

        // リセット
        currentFormulateActionIndex = 0;
        currentFormulateActionPhase = orderedFormulateActionsList[currentFormulateActionIndex];

        // 
        SetUpdateMethods(currentFormulateActionPhase);
    }

    /// <summary>
    /// 現在の工程から入力受付用関数とメインループ関数を指定
    /// </summary>
    /// <param name="actionType"></param>
    void SetUpdateMethods(FormulateActionType actionType) {
        // 入力受付用Update
        formulateActionUpdateMethod = GetActionDiscriminatorUpdateStateMethod(actionType);

        // メインループ用Update
        mainStateHandler.Initialize();
        mainUpdate = new UnityEvent();
        mainUpdate.RemoveAllListeners();
        // 関数指定
        switch (actionType) {
            case FormulateActionType.Rotate:
                mainUpdate.AddListener(mainStateHandler.StickRotate);
                break;
            case FormulateActionType.UpDown:
                mainUpdate.AddListener(mainStateHandler.StickUpDown);
                break;
            case FormulateActionType.ButtonMash:
                mainUpdate.AddListener(mainStateHandler.ButtonMash);
                break;
            case FormulateActionType.DPadPress:
                mainUpdate.AddListener(mainStateHandler.DPadPress);
                break;
            case FormulateActionType.ButtonLongPress:
                mainUpdate.AddListener(mainStateHandler.ButtonLongPress);
                break;
            case FormulateActionType.ButtonTiming:
                mainUpdate.AddListener(mainStateHandler.ButtonTiming);
                break;
            default:
                break;
        }
    }

    public FormulateActionUpdate GetActionDiscriminatorUpdateStateMethod(FormulateActionType actionType) {
        switch (actionType) {
            case FormulateActionType.Rotate:
                return stickRotateAction.UpdateState;
            case FormulateActionType.UpDown:
                return stickUpDownAction.UpdateState;
            case FormulateActionType.ButtonMash:
                return buttonPressingAction.UpdateState;
            case FormulateActionType.DPadPress:
                return dPadPressingAction.UpdateState;
            case FormulateActionType.ButtonLongPress:
                return buttonPressingAction.UpdateState;
            case FormulateActionType.ButtonTiming:
                return buttonPressingAction.UpdateState;
            default:
                return null;
        }
    }

    void FinishFormulating() {
        finish = true;
        Debug.Log("FINISH");
    }

    void FinishedUpdate() {
        if (input.fire0) {
            if (!runningExitProcess) {
                runningExitProcess = true;
                // 終了処理（一度だけ走る）

                if (phaseFailedCount>=2) {
                    Debug.Log("調合失敗");
                }

                ExitFormulateScene();
            }
            input.Fire0Input(false);
        }
    }

    void ExitFormulateScene() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("ResultScene");
    }
}