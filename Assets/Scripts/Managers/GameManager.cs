using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    [SerializeField] DataAccessUnit DAU;

    public PlayerController playerController;
    public InventoryManager inventoryManager;

    bool calledSceneMoveMethod = false;

    [Header("MainRenderer Canvas")]
    public bool mainRTCanvasActiveOnEditor;
    public GameObject mainRTCanvas;

    private void Awake() {
        Singleton();

        Initialize();
    }

    private void Singleton() {
        if (instance == null)
            instance = this;
        else if (instance != this)
            DestroyImmediate(this.gameObject);
    }

    private void Initialize() {
        if (mainRTCanvas) mainRTCanvas.SetActive(true);

        calledSceneMoveMethod = false;
    }

    public void GoNextScene() {
        if (calledSceneMoveMethod) return;

        if (DAU.data.ignoreDiagnose) {
            UnityEngine.SceneManagement.SceneManager.LoadScene("IngredientsSelectionScene");
        } else {
            UnityEngine.SceneManagement.SceneManager.LoadScene("DiagnoseScene");
        }

        calledSceneMoveMethod = true;
    }

    private void Update() {
#if UNITY_EDITOR
        if (UnityEngine.InputSystem.Keyboard.current.f3Key.wasPressedThisFrame) {
            foreach (var item in GetComponent<ItemRecipeDatabase>().AllIngredients) {
                inventoryManager.AddItemDataToInventory(item);
            }
            Debug.Log("[デバッグ機能] 全てのアイテムを付与しました");
        }
#endif
    }
}