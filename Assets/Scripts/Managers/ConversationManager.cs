using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ConversationManager : MonoBehaviour {

    public PlayerInputsReceiver input;

    [HideInInspector]
    public Diagnose.Patient.ScenarioLine[] scenarioLines;

    public Transform conversationLog_parent;
    public Transform conversationLog_content;
    public ScrollRect conversationLog_scrollRect;
    public ContentSizeFitter conversationLog_contentSizeFitter;
    public GameObject conversationLog_line_L;
    public GameObject conversationLog_line_R;

    // Start is called before the first frame update
    void Start() {
        conversationLog_parent.gameObject.SetActive(false);
    }

    public IEnumerator ConversationIE() {
        conversationLog_parent.gameObject.SetActive(true);

        int _index = 0;
        while (true) {
            if (input.submit || _index == 0) {
                if (_index < scenarioLines.Length) {
                    var scenario = scenarioLines[_index];
                    AddConversationLine(scenario);
                    _index++;
                } else if (_index == scenarioLines.Length) {
                    break;
                }

                // reset
                input.SubmitInput(false);
            }

            yield return null;
        }

        // Debug.Log("会話終了");

        conversationLog_parent.gameObject.SetActive(false);
        ClearConversationLogs();

        yield return new WaitForSeconds(0.5f);

        yield break;
    }

    GameObject AddConversationLine(Diagnose.Patient.ScenarioLine scenarioLine) {
        GameObject _go;
        if (scenarioLine.right) {
            _go = Instantiate(conversationLog_line_R, conversationLog_content);
        } else {
            _go = Instantiate(conversationLog_line_L, conversationLog_content);
        }

        TextMeshProUGUI guiText = _go.GetComponentInChildren<TextMeshProUGUI>();
        string _text = scenarioLine.line;

        guiText.text = _text;

        // ここでスクロールを一番下にする
        conversationLog_scrollRect.verticalNormalizedPosition = 0;
        // 高さ更新
        conversationLog_contentSizeFitter.SetLayoutVertical();

        return _go;
    }

    void ClearConversationLogs() {
        foreach (Transform child in conversationLog_content.transform) {
            Destroy(child.gameObject);
        }
    }
}