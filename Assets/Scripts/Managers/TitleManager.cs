using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour
{

    [SerializeField] Button startBtn;

    bool clicked = false;

    // Start is called before the first frame update
    void Start()
    {
        startBtn.Select();
        startBtn.onClick.AddListener(() => OnClickButton());

        clicked = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnClickButton()
    {
        if (!clicked)
        {
            SceneManager.LoadScene("PrototypeScene");
            clicked = true;
        }
    }
}