using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFootstepsSound : MonoBehaviour
{

    [SerializeField] AudioSource _as;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Play()
    {
        if (!_as.isPlaying) _as.Play();
    }

    public void Stop()
    {
        _as.Stop();
    }
}