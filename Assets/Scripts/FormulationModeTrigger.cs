using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormulationModeTrigger : MonoBehaviour {

    [SerializeField] UnityEngine.Events.UnityEvent action;

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) action?.Invoke();
    }
}