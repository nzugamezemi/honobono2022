using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEvent : MonoBehaviour {

    [SerializeField] UnityEvent m_event;

    public void RunEvent() {
        m_event?.Invoke();
    }
}