using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResultManager : MonoBehaviour {

    [SerializeField] DataAccessUnit DAU;

    public PlayerInputsReceiver input;

    [SerializeField] ConversationManager conversationManager;

    [SerializeField] GameObject formulatingPanel;
    [SerializeField] Animator formulatingPanelAnim;
    [SerializeField] Animator mainCanvasAnim;

    [SerializeField] Sprite failedMedicineSprite;

    [Header("患者")]
    [SerializeField] GameObject patientObj;
    [SerializeField] SpriteRenderer patientSR;
    [SerializeField] Animator patientAnim;

    [Header("受け渡し")]
    [SerializeField] Animator giveMedAnim;
    [SerializeField] SpriteRenderer medicineSR;
    [SerializeField] ParticleSystem medicineSuccessEff;
    [SerializeField] ParticleSystem medicineFailedEff;

    // Start is called before the first frame update
    IEnumerator Start() {
        formulatingPanel.SetActive(true);

        patientObj.SetActive(false);
        giveMedAnim.gameObject.SetActive(false);

        if (!DAU.data.patient) {
            Debug.LogError("患者データがありません"); yield break;
        } else {
            patientSR.sprite = DAU.data.patient._diseaseSprite;
            if (DAU.data.ExistMedicine) {
                // 作った薬の画像
                if (DAU.data.patient.medicine) medicineSR.sprite = DAU.data.GetCreatedMedicine()._icon;
            } else {
                // 薬　失敗画像
                medicineSR.sprite = failedMedicineSprite;
            }
        }

        patientObj.SetActive(true);
        giveMedAnim.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.5f);
        
        yield return new WaitUntil(() => formulatingPanelAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f);

        yield return new WaitForSeconds(1f);

        formulatingPanel.SetActive(false);

        yield return new WaitForSeconds(1f);

        // エフェクト再生
        {
            if (DAU.data.ExistMedicine) {
                medicineSuccessEff.Play();
            } else {
                medicineFailedEff.Play();
            }
        }

        yield return new WaitForSeconds(1f);

        giveMedAnim.Play("GiveMedicine");

        yield return null;

        yield return new WaitUntil(() => giveMedAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f);

        yield return new WaitForSeconds(0.5f);

        mainCanvasAnim.Play("ChangeFade");

        yield return null;

        yield return new WaitUntil(() => mainCanvasAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f);

        yield return new WaitForSeconds(1f);

        {   // シナリオセット
            if (DAU.data.formulationResultState == DataHolder.FormulatedMedicineResultState.Success) {
                conversationManager.scenarioLines = DAU.data.patient.receiveSuccessedMedicineScenarios;
            } else {
                conversationManager.scenarioLines = DAU.data.patient.receiveFailedMedicineScenarios;
            }
        }

        patientAnim.Play("SlidePatient");

        yield return new WaitForSeconds(0.1f);

        // 会話開始
        yield return StartCoroutine(conversationManager.ConversationIE());

        // データ共有初期化
        DAU.data.ResetAll();

        yield return new WaitForSeconds(0.1f);

        mainCanvasAnim.Play("EndFade");

        yield return null;

        yield return new WaitUntil(() => mainCanvasAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.99f);

        yield return new WaitForSeconds(0.5f);

        UnityEngine.SceneManagement.SceneManager.LoadScene("PrototypeScene");

        yield break;
    }

    public void ChangeNormalPatient() {
        if (DAU.data.formulationResultState == DataHolder.FormulatedMedicineResultState.Success) {
            patientSR.sprite = DAU.data.patient._goodSprite;
        } else {
            patientSR.sprite = DAU.data.patient._badSprite;
        }
    }
}