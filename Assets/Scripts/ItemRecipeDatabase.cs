using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ItemRecipeDatabase : MonoBehaviour {

    [SerializeField] private List<Recipe> allRecipes = new List<Recipe>();
    public List<Recipe> AllRecipes => allRecipes;
    public Recipe GetRecipeData(int databaseIndex) => allRecipes[databaseIndex];
    public Recipe GetRecipeDataFromMedicineData(Item medicine) {
        return AllRecipes.Where(i => i.result == medicine).FirstOrDefault();
    }

    [SerializeField] private List<Item> allIngredients = new List<Item>();
    public List<Item> AllIngredients => allIngredients;
    public Item GetIngredientData(int databaseIndex) => AllIngredients[databaseIndex];
    public int GetIngredientIndexFromItemData(Item item) => AllIngredients.IndexOf(item);

    [SerializeField] private List<Item> allMedicines = new List<Item>();
    public List<Item> AllMedicines => allMedicines;
    public Item GetMedicineData(int databaseIndex) => AllMedicines[databaseIndex];
    public int GetMedicineIndexFromItemData(Item item) => AllMedicines.IndexOf(item);

    public enum MedicineType {
        None = -1,
        Powdered,   // 粉
        Syrup,   // シロップ
        Tablet,   // 錠
        Cream,   // クリーム
        Aroma_Solid,   // アロマ固体
        Aroma_Liquid   // アロマ液体
    }

    public Recipe GetRecipeDataFromIngredients(List<Item> ingredientsList) {
        List<Recipe> _allRecipeList = AllRecipes;

        List<Item> _sortedIngredientsList = new List<Item>(ingredientsList);
        _sortedIngredientsList.Sort((a, b) => string.Compare(a._name, b._name));

        // bool _result = false;
        Recipe _result = null;

        if (_allRecipeList == null || _allRecipeList.Count == 0) {
            return _result;
        }

        foreach (var recipe in _allRecipeList) {
            if (recipe == null) continue;

            if (ingredientsList.Count != recipe.ingredients.Count) {
                Debug.LogError("材料の数がレシピと一致していません");
                continue;
            }

            List<Item> _sortedRecipeIngredients = recipe.GetSortedIngredients();

            if (_sortedRecipeIngredients.SequenceEqual(_sortedIngredientsList)) {
                // _result = true;
                _result = recipe;
                //Debug.Log("<color=#00e5ff>一致するレシピが見つかりました</color>");
                break;
            } else {
                continue;
            }
        }

        //if (_result == null) Debug.Log("<color=#00e5ff>一致するレシピが見つかりませんでした</color>");

        return _result;
    }
}