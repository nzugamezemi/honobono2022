using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class InventoryManager : MonoBehaviour {

    [SerializeField] DataAccessUnit DAU;

    public List<SingleInventoryData> invData => DAU.data.playerInventory;

    [Serializable]
    public class SingleInventoryData {
        public Item itemData;   // アイテム情報
        public int itemNumber = 1;   // アイテムの所持数

        public SingleInventoryData(Item itemData, int itemNumber = 1) {
            this.itemData = itemData ?? throw new ArgumentNullException(nameof(itemData));
            this.itemNumber = itemNumber;
        }
    }

    private void Start()
    {
        // invData.Clear();
    }

    public void AddItemDataToInventory(Item itemData)
    {
        // 持ってるかチェック
        bool alreadyHave = invData.Any(i => i.itemData == itemData);

        if (alreadyHave)
        {
            Debug.Log("既に持っています");
            return;
        }

        invData.Add(new SingleInventoryData(itemData));
    }
}