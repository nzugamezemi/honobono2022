using UnityEngine;
using System.Collections;
using TMPro;

public class TextController : MonoBehaviour {

	[SerializeField] PlayerInputsReceiver input;
	private bool isRejectingInput;
	public void RejectInput() { if (!isRejectingInput) isRejectingInput = true; }
	public void AcceptInput() { if (isRejectingInput) isRejectingInput = false; }

	public bool isTexting = false;

	public string[] scenarios;
	[SerializeField] TextMeshProUGUI uiText;

	[SerializeField]
	[Range(0.001f, 0.3f)]
	float intervalForCharacterDisplay = 0.05f;

	private string currentText = string.Empty;
	private float timeUntilDisplay = 0;
	private float timeElapsed = 1;
	private int currentLine = 0;
	private int lastUpdateCharacter = -1;

	// 文字の表示が完了しているかどうか
	public bool IsCompleteDisplayText {
		get { return Time.time > timeElapsed + timeUntilDisplay; }
	}

	private void Start() {
		ResetScenarios();
	}

	public void StartTexting() {
		SetNextLine();
		isTexting = true;
	}

	void EndTexting() {
		isTexting = false;
		ResetScenarios();
	}

	void Update() {
		if (isTexting) {
			if (isRejectingInput) input.SubmitInput(false);

			// クリック時に次の行を表示する
			if (input.submit && !isRejectingInput) {
				// 文字の表示が完了している
				if (IsCompleteDisplayText) {
					if (currentLine < scenarios.Length) {
						SetNextLine();
					} else {
						EndTexting();
					}
				} else {
					// 完了してないなら文字をすべて表示する
					timeUntilDisplay = 0;
				}

				Debug.Log("ボタン押下");

				// reset submit input
				input.SubmitInput(false);
			}

			int displayCharacterCount = (int)(Mathf.Clamp01((Time.time - timeElapsed) / timeUntilDisplay) * currentText.Length);
			if (displayCharacterCount != lastUpdateCharacter && displayCharacterCount > 0) {
				uiText.text = currentText.Substring(0, displayCharacterCount);
				lastUpdateCharacter = displayCharacterCount;
			}
		}
	}

	void SetNextLine() {
		if (scenarios == null || scenarios.Length == 0) return;

		currentText = scenarios[currentLine];
		timeUntilDisplay = currentText.Length * intervalForCharacterDisplay;
		timeElapsed = Time.time;
		currentLine++;
		lastUpdateCharacter = -1;
	}

	public void ResetScenarios() {
		scenarios = null;
		currentLine = 0;
		currentText = string.Empty;
		uiText.text = "";
		timeUntilDisplay = 0;
		timeElapsed = 1;
		currentLine = 0;
		lastUpdateCharacter = -1;

		RejectInput();

		Debug.Log("TextController 初期化");
	}
}