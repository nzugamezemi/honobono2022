using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class MedicinalPlantCollectPointHandler : MonoBehaviour {

    public enum GrowthStage {
        None = -1,
        Still,
        First,
        Second,
        Third
    }

    GrowthStage currentGrowthStage = GrowthStage.None;

    GrowthStage maxGrothStage = GrowthStage.Third;

    public bool StageComplete() => currentGrowthStage == maxGrothStage;

    float timeSpanStillToFirst = 1f;
    float timeSpanFirstToSecond = 1f;
    float timeSpanSecondToThird = 1f;

    Sprite firstStageSprite;
    Sprite secondStageSprite;
    Sprite thirdStageSprite;

    Item returnItem;

    private MedicinalPlantData data;

    [Header("薬草オブジェクトデータリスト")]
    public List<MedicinalPlantData> medicinalPlantDataList;

    [Header("コールバック")]
    public UnityEngine.Events.UnityEvent onGrowthStageChanged;
    public UnityEngine.Events.UnityEvent onItemPickedUp;

    float stageElapsedTime;
    float stageTimerMaxCount;

    bool inArea = false;

    [Header("参照")]
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] SphereCollider collider;
    [SerializeField] ParticleSystem particle;
    [SerializeField] Animator worldUIAnim;
    [SerializeField] TextMeshProUGUI worldUIText;

    private void Start() => Initialize();

    /// <summary>
    /// 初期化処理
    /// </summary>
    void Initialize() {
        worldUIAnim = GetComponent<Animator>();
        collider = GetComponent<SphereCollider>();
        collider.isTrigger = true;

        data = null;

        // データランダム化
        DataRandomize();

        stageElapsedTime = 0;

        inArea = false;

        // 成長段階を最終で上書き
        OverrideGrowthStage(maxGrothStage);
    }

    void DataRandomize() {
        var list = medicinalPlantDataList;
        data = list[UnityEngine.Random.Range(0, list.Count)];

        if (data == null) Debug.LogError("データを参照できません");

        // データ取得
        returnItem = data.item;
        timeSpanStillToFirst = data.timeSpanStillToFirst;
        timeSpanFirstToSecond = data.timeSpanFirstToSecond;
        timeSpanSecondToThird = data.timeSpanSecondToThird;
        if (data.firstStageSprite) firstStageSprite = data.firstStageSprite;
        if (data.secondStageSprite) secondStageSprite = data.secondStageSprite;
        if (data.thirdStageSprite) thirdStageSprite = data.thirdStageSprite;
        maxGrothStage = data.maxGrothStage;

        worldUIText.text = returnItem._name.ToString();
    }

    private void Update() {
        // タイマー
        if (stageElapsedTime <= stageTimerMaxCount) stageElapsedTime += Time.deltaTime;

        if (stageTimerMaxCount != -1 && stageElapsedTime >= stageTimerMaxCount && (int)currentGrowthStage < (int)GrowthStage.Third) {
            // 次の段階へ
            int nextStageIndex = (int)currentGrowthStage + 1;
            GrowthStage nextStage = (GrowthStage)Enum.ToObject(typeof(GrowthStage), nextStageIndex);
            OverrideGrowthStage(nextStage);

            // タイマーリセット
            stageTimerMaxCount = GetTimeSpan(currentGrowthStage);
            stageElapsedTime = 0;
        }

        if (GameManager.instance.playerController.input.fire0 && inArea) {
            worldUIAnim.SetBool("visible", false);
            inArea = false;
            var item = PickUp();
            if (item != null) GameManager.instance.inventoryManager.AddItemDataToInventory(item);
            // reset
            GameManager.instance.playerController.input.Fire0Input(false);
        }
    }

    void OverrideGrowthStage(GrowthStage newStage) {
        if (newStage != GrowthStage.Third) {
            particle.Stop();
        }

        switch (newStage) {
            case GrowthStage.None:
                break;
            case GrowthStage.Still:
                // スプライト更新
                if (spriteRenderer)
                    spriteRenderer.sprite = null;
                break;
            case GrowthStage.First:
                // スプライト更新
                if (spriteRenderer && firstStageSprite)
                    spriteRenderer.sprite = firstStageSprite;
                break;
            case GrowthStage.Second:
                // スプライト更新
                if (spriteRenderer && secondStageSprite)
                    spriteRenderer.sprite = secondStageSprite;
                break;
            case GrowthStage.Third:
                // スプライト更新
                if (spriteRenderer && thirdStageSprite)
                    spriteRenderer.sprite = thirdStageSprite;
                particle.Play();
                collider.enabled = true;
                break;
        }

        currentGrowthStage = newStage;

        // コールバック
        onGrowthStageChanged?.Invoke();
    }

    float GetTimeSpan(GrowthStage stage) {
        float _timespan = -1;
        switch (stage) {
            case GrowthStage.None:
                break;
            case GrowthStage.Still:
                _timespan = timeSpanStillToFirst;
                break;
            case GrowthStage.First:
                _timespan = timeSpanFirstToSecond;
                break;
            case GrowthStage.Second:
                _timespan = timeSpanSecondToThird;
                break;
            case GrowthStage.Third:
                break;
        }

        return _timespan;
    }

    public Item PickUp() {
        // 成長してるか
        if (!StageComplete()) return null;

        // 成長段階リセット
        OverrideGrowthStage(GrowthStage.Still);

        // タイマーリセット
        stageTimerMaxCount = GetTimeSpan(currentGrowthStage);
        stageElapsedTime = 0;

        // コライダー非アクティブ
        collider.enabled = false;

        // データランダム化
        Invoke("DataRandomize", 0.1f);

        onItemPickedUp?.Invoke();
        return returnItem;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            if (StageComplete()) {
                worldUIAnim.SetBool("visible", true);
            }
            inArea = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")) {
            worldUIAnim.SetBool("visible", false);
            inArea = false;
        }
    }
}