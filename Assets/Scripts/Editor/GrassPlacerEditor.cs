using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GrassPlacer))]
public class GrassTestEditor : Editor {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        GrassPlacer data = target as GrassPlacer;

        if (GUILayout.Button("����")) {
            data.PlaceGrass();
        }

        if (GUILayout.Button("�폜")) {
            data.DestroyChildren(data.transform);
        }
    }
}