using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputsReceiver : MonoBehaviour {

	[Header("Player Input Values")]
	public Vector2 rawMove;
	public Vector2 move;
	public Vector2 moveInputMagnitude;
	public float inputAcceleration = 4, inputDeceleration = 4;
	public Vector2 look;
	public bool jump;
	public bool sprint;
	public bool fire0;

	// UI
	public bool submit;
	public bool cancel;

	public void OnMove (InputAction.CallbackContext context) {
		MoveInput(context.ReadValue<Vector2>());
	}

	public void OnLook (InputAction.CallbackContext context) {
		LookInput(context.ReadValue<Vector2>());
	}

	public void OnJump (InputAction.CallbackContext context) {
		JumpInput(context.performed);
	}

	public void OnSprint (InputAction.CallbackContext context) {
		SprintInput(context.performed);
	}

	public void OnFire0(InputAction.CallbackContext context) {
		Fire0Input(context.performed);
	}

	public void OnSubmit(InputAction.CallbackContext context) {
		SubmitInput(context.performed);
	}

	public void OnCancel(InputAction.CallbackContext context) {
		CancelInput(context.performed);
	}

	public void MoveInput (Vector2 newMoveDirection) {
		rawMove = ClampedRawInput(newMoveDirection);
	}

	public void LookInput (Vector2 newLookDirection) {
		look = newLookDirection;
	}

	public void JumpInput(bool newJumpState) {
		jump = newJumpState;
	}

	public void SprintInput (bool newSprintState) {
		sprint = newSprintState;
	}

	public void Fire0Input(bool newFire0State) {
		fire0 = newFire0State;
	}

	public void SubmitInput (bool newSubmitState) {
		submit = newSubmitState;
	}

	public void CancelInput (bool newCancelState) {
		cancel = newCancelState;
	}

	public void ResetMoveInput () {
		rawMove = move = Vector2.zero;
    }

	public Vector2 GetLerpedInputValue () {
		if (Mathf.Abs(rawMove.magnitude - move.magnitude) > 0.01f) {
			move = Vector2.Lerp(move, rawMove, Time.deltaTime * (rawMove.magnitude > 0.001f ? inputAcceleration : inputDeceleration));
			return move;
		} else {
			move = rawMove;
			return move;
		}
	}

	Vector2 ClampedRawInput (Vector2 input) {
		input.x = Mathf.Clamp(input.x, -1, 1);
		input.y = Mathf.Clamp(input.y, -1, 1);
		return input;
	}
}