using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class StartFormulateButtonSelectHandler : MonoBehaviour, ISelectHandler {

    public UnityEvent onSelect;
    public UnityEvent onDeselect;

    public void OnSelect(BaseEventData eventData) {
        onSelect?.Invoke();
    }

    public void OnDeselect(BaseEventData eventData) {
        onDeselect?.Invoke();
    }
}